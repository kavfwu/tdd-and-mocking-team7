package LoginService;

class LoginService {
	   private final IAccountRepository accountRepository;
	   
	   public LoginService(IAccountRepository accountRepository) {
	      this.accountRepository = accountRepository;
	   }
	 
	   private int failedAttempts = 0;
	   private String previousAccountId = "";
	 
	   public void login(String accountId, String password) {
	      IAccount account = accountRepository.find(accountId);
	      
	      if (account == null)
	         throw new AccountNotFoundException();
	      
	      if (account.passwordMatches(password)){
	          if (account.isLoggedIn())
	              throw new AccountLoginLimitReachedException();        
	          if (account.isRevoked())
	              throw new AccountRevokedException();
	          if (account.isExpired())
	              throw new AccountPasswordExpiredException();        
	          if (account.isTemporary())
	              throw new AccountPasswordIsTemporaryException();
	    	  account.setLoggedIn(true);
	      }
	      else{
	          if (previousAccountId.equals(accountId))
	              ++failedAttempts;
	           else {
	              failedAttempts = 1;
	              previousAccountId = accountId;
	           }
	      }
	      if (failedAttempts == 3)
	         account.setRevoked(true);
	   }
	   
		public boolean changePassword(String accountId, String password){
			IAccount account = accountRepository.find(accountId);
			if (account == null)
				throw new AccountNotFoundException();
			if (account.within24PrevPwds(password))
				return false;
			account.setNewPassword(password);
			return true;
			}
		

}
