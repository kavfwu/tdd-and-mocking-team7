package LoginService;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;

public class LoginServiceTest {
  private IAccount account;
  private IAccountRepository accountRepository;
  private LoginService service;

  @Before
  public void init() {
     account = mock(IAccount.class);
     accountRepository = mock(IAccountRepository.class);
     when(accountRepository.find(anyString())).thenReturn(account);
     service = new LoginService(accountRepository);
  }

  private void willPasswordMatch(boolean value) {
      when(account.passwordMatches(anyString())).thenReturn(value);
   }
 
   @Test
   public void itShouldSetAccountToLoggedInWhenPasswordMatches() {
      willPasswordMatch(true);
      service.login("brett", "password");
      verify(account, times(1)).setLoggedIn(true);
   }
 
   @Test
   public void itShouldSetAccountToRevokedAfterThreeFailedLoginAttempts() {
      willPasswordMatch(false);
 
      for (int i = 0; i < 3; ++i)
         service.login("brett", "password");
 
      verify(account, times(1)).setRevoked(true);
   }
   
   @Test
   public void itShouldNotSetAccountLoggedInIfPasswordDoesNotMatch() {
      willPasswordMatch(false);
      service.login("brett", "password");
      verify(account, never()).setLoggedIn(true);
   }
   
   @Test
   public void itShouldNotRevokeSecondAccountAfterTwoFailedAttemptsFirstAccount() {
      willPasswordMatch(false);
 
      IAccount secondAccount = mock(IAccount.class);
      when(secondAccount.passwordMatches(anyString())).thenReturn(false);
      when(accountRepository.find("schuchert")).thenReturn(secondAccount);
 
      service.login("brett", "password");
      service.login("brett", "password");
      service.login("schuchert", "password");
 
      verify(secondAccount, never()).setRevoked(true);
   }
   
   @Test(expected = AccountLoginLimitReachedException.class)
   public void itShouldNowAllowConcurrentLogins() {
      willPasswordMatch(true);
      when(account.isLoggedIn()).thenReturn(true);
      service.login("brett", "password");
   }
   
   @Test(expected = AccountNotFoundException.class)
   public void ItShouldThrowExceptionIfAccountNotFound() {
      when(accountRepository.find("schuchert")).thenReturn(null);
      service.login("schuchert", "password");
   }
   
   @Test(expected = AccountRevokedException.class)
   public void ItShouldNotBePossibleToLogIntoRevokedAccount() {
      willPasswordMatch(true);
      when(account.isRevoked()).thenReturn(true);
      service.login("brett", "password");
   }
  
  // test for the feature that users cannot login to account with expired password
  @Test(expected = AccountPasswordExpiredException.class)
  public void CannotLogintoAccountwithExpiredPassword(){
	  willPasswordMatch(true);
	  when(account.isExpired()).thenReturn(true);
	  service.login("brett", "password");
  }
  
  // test for the feature that users can login to account with expired password after changing the password
  @Test
  public void CanLogintoAccountwithExpiredPasswordAfterChangingthePassword(){
	  willPasswordMatch(true);
	  when(account.isExpired()).thenReturn(true);
	  when(account.within24PrevPwds(anyString())).thenReturn(false);
	  if(service.changePassword("brett","password"))
		  when(account.isExpired()).thenReturn(false);
	  service.login("brett", "password");
      verify(account, times(1)).setLoggedIn(true);
  }
  
  // test for the feature that users cannot login to account with temporary password
  @Test( expected = AccountPasswordIsTemporaryException.class)
  public void CannotLogintoAccountwithTemporaryPassword(){
	  willPasswordMatch(true);
	  when(account.isTemporary()).thenReturn(true);
	  service.login("brett", "password");
  }
  
  // test for the feature that users can login to account with temporary password after changing the password
  @Test
  public void CanLogintoAccountwithTemporaryPasswordAfterChangingthePassword(){
	  willPasswordMatch(true);
	  when(account.isTemporary()).thenReturn(true);
	  when(account.within24PrevPwds(anyString())).thenReturn(false);
	  if(service.changePassword("brett","password"))
		  when(account.isTemporary()).thenReturn(false);
	  service.login("brett", "password");
      verify(account, times(1)).setLoggedIn(true);
  }
  
  //test for the feature that users cannot change password to any of previous 24 passwords
  @Test
  public void CannotChangePasswordtpAnyofPrevious24Passwords(){
	  when(account.within24PrevPwds(anyString())).thenReturn(true);
	  service.changePassword("brett","password");
	  verify(account, never()).setNewPassword("password");
  }
  //test for the feature that users can change password to previous password if > 24 changes from last use
  @Test
  public void CanChangePasswordifisnotoneofprevious24(){
	  when(account.within24PrevPwds(anyString())).thenReturn(false); 
	  service.changePassword("brett","password");
	  verify(account, times(1)).setNewPassword("password");
  }
  
}