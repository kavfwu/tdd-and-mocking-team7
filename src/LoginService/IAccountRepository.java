package LoginService;

interface IAccountRepository {
    IAccount find(String accountId);
}
