package LoginService;

public interface IAccount {
	void setLoggedIn(boolean value);
	boolean passwordMatches(String candidate);
	void setRevoked(boolean value);
	boolean isLoggedIn();
	boolean isRevoked();
	boolean isExpired();
	boolean within24PrevPwds(String PassWord);
	boolean isTemporary();
	void setNewPassword(String newPassWord);
}
